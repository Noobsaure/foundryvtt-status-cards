export class Clock {
    static sizes = [
        16
    ];

    static image = {
        width: 350,
        height: 75
    };

    static themePath = 'modules/progress-clocks/themes';

    static themes = {
        city_of_mist: `${Clock.themePath}/com`
    };

    constructor ({ size, progress, theme } = {}) {
        const isSupportedSize = size && Clock.sizes.indexOf(parseInt(size)) >= 0;

        this.size = isSupportedSize ? parseInt(size) : Clock.sizes[0];

        if (!progress || progress < 0) {
            this.progress = 0;
        } else if (progress > size) {
            this.progress = this.size;
        } else {
            this.progress = progress;
        }

        this.theme = theme || 'city_of_mist';
    }

    increment () {
        const old = this;
        return new Clock({
            size: old.size,
            progress: old.progress + 1,
            theme: old.theme
        });
    }

    decrement () {
        const old = this;
        return new Clock({
            size: old.size,
            progress: old.progress - 1,
            theme: old.theme
        });
    }
}
